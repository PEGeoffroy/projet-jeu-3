export class Player{
  constructor(image, life, passiveSkill){
    this.image = image;
    this.life = life;
    this.passiveSkill = passiveSkill;
  }

  looseLife(){
    this.life--;
    this.displayLife;
  }
  gainLife(){
    this.life++;
    this.displayLife;
  }
  displayLife(){
    console.log(`${this.life}`);
  }
}