"use strict";

export class Item{
  constructor(name, kind, color, isClue){
    this.name = name;
    this.kind = kind;
    this.color = color;
    this.isClue = isClue;
    this.image = `./images/${this.kind}/${this.name}.png`;
  }
}